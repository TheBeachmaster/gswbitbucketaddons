# gswbitbucketaddons

Get Started with Bitbucket Addons

### TL;DR;

> Use `sudo` if necessary

```bash
$ npm i -g atlas-connect
```

```bash
$ git clone https://TheBeachmaster@bitbucket.org/TheBeachmaster/gswbitbucketaddons.git
```

```bash
$ cd gswbitbucketaddons
```

```bash
$ git checkout master
```

```bash
$ npm install --save
```

> Create a new file within the `augnairobi` directory called `credentials.json` and add the following contents and save

```javascript
{
    "bitbucket": {
        "username": "username", // bitbucket username
        "password": "password" // bitbucket password
    }
}

```

```bash
$ npm start
```

> Startup `ngrok` on port `3000` (default) / or other

```bash
$ ngrok http 3000
```

> In your project directory edit the `config.json` and edit the `consumerKey` section to look like such

```javascript
 "consumerKey": "your_consumer_key" // Get Key from Bitbucket Settings
```

> Go to Bitbucket `Settings  >  Manage Integration`  Then Select `Install Add-on from URL`

![install](imgs/install.PNG)

> Add the ngrok URL e.g. `https://abdefg.ngrok.io` 
>
> Ensure your Consumer key is mapped to your add-on URL

### Deploy to Prod

> By default, for NodeJS deployments default environment is `$NODE_ENV=production`, your `production` section of your `config.json` will be used.
>
> By default Heroku `port` will be used :  no need to change this section 
>
> The add-on will be invoked from your default Heroku url , this will be re-routed to your `atlassian-connect.json` which is app manifest file.
>
> You can also configure your data persistence service (e.g. Free Heroku Postgres instance to store sessions and users)

```javascript
    "production": {
        "port": "$PORT",
        "errorTemplate": true,
        "consumerKey": "ConsumerKey",
        "localBaseUrl": "https://appurl.herokuapp.com/",
        "store": {
            "type": "postgres",
            "url": "postgres://url/auth"
        },
        "whitelist": [
            "bitbucket.org"
        ]
    }
```

> Application interactions are by default initiated from `public/js/addon.js` 